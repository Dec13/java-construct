

# [求Star！点击在线阅读](https://gugugubo.github.io/javaLearing/)


- [Java](#java)
  - [基础](#基础)
  - [容器](#容器)
  - [并发](#并发)
  - [JVM](#jvm)
- [网络](#网络)
- [操作系统](#操作系统)
- [数据结构与算法](#数据结构与算法)
  - [数据结构](#数据结构)
  - [算法](#算法)
- [数据库](#数据库)
  - [MySQL](#mysql)
  - [Redis](#redis)
- [系统设计](#系统设计)
  - [项目实战](#项目实战)
  - [编码之道](#编码之道)
  - [常用框架](#常用框架)
    - [Spring/SpringBoot](#springspringboot)
    - [MyBatis](#mybatis)
    - [Netty](#netty)
  - [认证授权](#认证授权)
    - [JWT](#jwt)
    - [springSercurity](#springSercurity)
    - [SSO(单点登录)](#sso单点登录)
  - [分布式](#分布式)
    - [搜索引擎](#搜索引擎)
    - [RPC](#rpc)
    - [API 网关](#api-网关)
    - [分布式 id](#分布式-id)
    - [ZooKeeper](#zookeeper)
  - [微服务](#微服务)
  - [高并发](#高并发)
    - [消息队列](#消息队列)
    - [读写分离](#读写分离)
    - [分库分表](#分库分表)
    - [负载均衡](#负载均衡)
  - [高可用](#高可用)
    - [CAP 理论](#cap-理论)
    - [BASE 理论](#base-理论)
    - [限流](#限流)
    - [降级](#降级)
    - [熔断](#熔断)
    - [排队](#排队)
  - [大型网站架构](#大型网站架构)
- [工具](#工具)



## Java

### 基础



### 容器
1. **[HashMap](java集合/HashMap.md)**
2. **[并发集合](java集合/并发集合.md)**



### 并发
1. **[synchronized详解和park(),unpark()方法](java并发编程/java并发1.md)**
2. **[volatile 和 cas](java并发编程/java并发2.md)**
3. **[线程池和ReadWriteLock和juc下其它组件](java并发编程/并发3.md)**

一些javadoc文档的翻译
1. **[Condition类文档翻译](java并发编程/javadoc文档/condition.md)**
2. **[Lock类文档翻译](java并发编程/javadoc文档/Lock.md)**
3. **[Thread和Runnable类文档翻译](java并发编程/javadoc文档/Thread和Runnable.md)**
4. **[wait()和notify()方法文档翻译](java并发编程/javadoc文档/wait()和notify()方法.md)**


### JVM
1. **[jvm-垃圾回收](jvm学习/jvm-垃圾回收.md)**
2. **[jvm-类加载过程](jvm学习/jvm-类加载过程)**
3. **[jvm-内存结构](jvm学习/jvm-内存结构.md)**
4. **[jvm-字节码](jvm学习/jvm-字节码.md)**
5. **[G1详解](jvm学习/SubFolder/G1详解.md)**
6. [**jvm-一天一夜搞懂String Pool字符串常量池**](jvm学习/SubFolder/jvm-一天一夜搞懂StringPool字符串常量池.md)




## 网络
看《计算机网络自顶向下方法》
1. **[计网笔记](https://bithachi.blog.csdn.net/article/details/104722679)**


## 操作系统
看《现代操作系统》
1. **[操作系统笔记](https://bithachi.blog.csdn.net/article/details/104415990)**


## 数据结构与算法

### 数据结构
1. **[labuladong的算法](https://github.com/labuladong/fucking-algorithm)**


### 算法
1. **[labuladong的算法](https://github.com/labuladong/fucking-algorithm)**



## 数据库

### MySQL
《深入浅出mysql》
1. **[sql语言基础](数据库/mysql/深入浅出mysql读书笔记/第二章sql基础.md)**
2. **[建表必了解的数据类型1](数据库/mysql/深入浅出mysql读书笔记/第三章mysql支持的数据类型.md)**
3. **[建表必了解的数据类型2](数据库/mysql/深入浅出mysql读书笔记/第八章选择合适的数据类型.md)**
4. **[储存引擎](数据库/mysql/深入浅出mysql读书笔记/第七章储存引擎的选择.md)**
5. **[sql优化](数据库/mysql/深入浅出mysql读书笔记/第十八章SQL优化.md)**
6. **[锁问题](数据库/mysql/深入浅出mysql读书笔记/第二十章锁问题.md)**

其它
1. **[事务详解](数据库/mysql/事务详解.md)**
2. **[索引详解](数据库/mysql/MySql视频.md)**




### Redis
《Redis实战》
基础
1. **[数据结构和内部编码](数据库/redis/数据结构和内部编码.md)**
2. **[持久化](数据库/redis/持久化.md)**
3. **[缓存设计](数据库/redis/缓存设计.md)**
4. **[内存管理](数据库/redis/内存管理.md)**

高级
1. **[集群](数据库/redis/集群.md)**
2. **[复制](数据库/redis/复制.md)**
3. **[哨兵](数据库/redis/哨兵.md)**

## 系统设计

### 项目实战
1. **[如何上手mall-tiny项目？](搞七搞八/mall-tiny项目学习.md)**

### 编码之道

### 常用框架

#### Spring/SpringBoot

#### MyBatis

#### Netty

### 认证授权

#### springSercurity
1. **[SpringSecurity上手与源码解析](框架学习/SpringSecurity/权限管理.md)**



#### JWT

#### SSO(单点登录)

### 分布式

#### 搜索引擎

#### RPC

#### API 网关

#### 分布式 id

#### ZooKeeper

### 微服务

### 高并发

#### 消息队列

#### 读写分离


#### 分库分表


#### 负载均衡


### 高可用


#### CAP 理论

#### BASE 理论


#### 限流


#### 降级



#### 熔断


#### 排队


#### 集群


#### 超时和重试机制


### 大型网站架构


## 工具
